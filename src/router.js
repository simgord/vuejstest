import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
// import User from './views/User.vue';
import Dashboard from './views/Dashboard.vue';
import DashboardMonitoring from './views/DashboardMonitoring.vue';
import DashboardPreferences from './views/DashboardPreferences.vue';
import Login from './views/Login.vue';
import Signup from './views/Signup.vue';


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
    },
    {
      path: '/dashboard/monit',
      name: 'dashmonitor',
      component: DashboardMonitoring,
    },
    {
      path: '/dashboard/pref',
      name: 'dashpref',
      component: DashboardPreferences,
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    // },
    {
      path: '/contact',
      name: 'contact',
      component: () => import('./views/Contact.vue'),
    },
  ],
});
